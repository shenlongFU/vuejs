'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_KEY: '"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhpZXVuZEBtYWlsLmNvbSIsIl9pZCI6IjYzMWRlNTY3MjVkMTQ3NDY5Y2I4YWUwZiIsInJvbGUiOiJhZG1pbiIsImlhdCI6MTY2MzEyMTUxMCwiZXhwIjoxNjYzMTY0NzEwfQ.idESTSPgBTFrSIJd4YyRD3ByY46nF1KXVgqUflVeqik"'
})
