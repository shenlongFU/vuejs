



const app= new Vue({
            el: '#table-content',
            data:{
                posts: []
            },
            created(){
                    fetch('http://localhost/cakephp/posts/show')
                    .then(response => response.json())
                    .then(json=>{
                         this.posts= json.data
                    })
            },
            methods:{
                fireDelete(postid,index){
                    var url = 'http://localhost/cakephp/posts/remove';
                    var data = {id: postid};
                    fetch(url, {
                        method: 'POST', // or 'PUT'
                        body: JSON.stringify(data) // data can be `string` or {object}!
                    }).then(res => res.json())
                        .then(response => console.log('Success:', JSON.stringify(response)))
                        .then( this.$delete(this.posts, index))
                        .catch(error => console.error('Error:', error));
                }

            }

})
