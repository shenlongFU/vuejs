Vue.component('sidebar', {
    template: '<div>' +
        '<div class="logo">' +
        '<div id="app">' +
        '<a href="http://www.creative-tim.com" class="simple-text logo-mini"><img src="../assets/img/logo-small.png"></a>' +
        '<a href="http://www.creative-tim.com" class="simple-text logo-normal">MasterDev HieuND</a>' +
        '</div>' +
        '</div>' +
        '<div class="sidebar-wrapper">' +
        '<ul class="nav">' +
        '<li class="active ">' +
        '<a href="./tables.html">' +
        '<i class="nc-icon nc-tile-56"></i>' +
        '<p>Danh sách bài viết</p>' +
        '</a>' +
        '</li>' +
        '</ul>' +
        '</div>' +
        '</div>'
});

new Vue({
    el: '#side',
    data: {
        message: 'Hello Vue.js!'
    }
});
