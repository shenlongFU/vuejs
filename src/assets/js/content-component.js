Vue.component('container', {
  props: ['posts'],
  data: function () {
    return {
      mess: 'this is content'
    }
  },
  created () {
    fetch('http://localhost/cakephp/posts/show')
      .then(response => response.json())
      .then(json => {
        this.posts = json.data
      })
  },
  methods: {
    fireDelete (postid, index) {
      var url = 'http://localhost/cakephp/posts/remove'
      var data = {id: postid}
      fetch(url, {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data) // data can be `string` or {object}!
      }).then(res => res.json())
        .then(response => console.log('Success:', JSON.stringify(response)))
        .then(this.$delete(this.posts, index))
        .catch(error => console.error('Error:', error))
    }

  },
  template: ' <div class="card"> ' +
        ' <div class="card-header col-md-12">' +
        '<div class="col-md-4">' +
        ' <div class="input-group-text">' +
        ' <input type="text" value="" class="form-control" placeholder="Search...">' +
        ' <i class="nc-icon nc-zoom-split"></i>' +
        '  </div>' +
        '  </div>' +
        '  <div class="col-md-4">' +
        '  <button class="btn btn-primary" > Add new user</button>' +
        '</div>' +
        '  </div>' +
        ' <div class="card-body">' +
        ' <div class="table-responsive">' +
        ' <table class="table " style="table-layout:fixed;">' +
        ' <thead class=" text-primary">' +
        '  <th>' +
        '   Tiêu đề bài viết' +
        '</th>' +
        '<th>' +
        'Ngày tạo' +
        '</th>' +
        '<th>' +
        'Nội dung' +
        '</th>' +
        '<th class="text-right">' +
        'Thao tác' +
        '</th>' +
        '</thead>' +
        '<tbody id="table-content">' +
        '   <tr v-for="(post,index) in posts" >' +
        '  <td style=" overflow: none;">' +
        '{{post.Post.title}}' +
        '</td>' +
        '<td>' +
        '{{post.Post.created}}' +
        '</td>' +
        '<td style="overflow:hidden;white-space:nowrap;' +
        'text-overflow: ellipsis ">' +
        '{{post.Post.body}}' +
        '</td>' +
        '<td class="text-right">' +
        '   <i class="nc-icon nc-ruler-pencil"></i>' +
        '  <a href="https://www.lazada.vn">Edit</a>' +
        ' <i class="nc-icon nc-simple-remove"></i>' +
        ' <button v-on:click="fireDelete(post.Post.id,index)">Delete</button>' +
        '  </td>' +
        '  </tr>' +
        '  </tbody>' +
        '  </table>' +
        ' </div>' +
        ' </div>' +
        ' </div>'

})

new Vue({
  el: '#content',
  data: {
    message: 'Hello Vue.js!'
  }

})
