import Vue from 'vue'
import Router from 'vue-router'

import Content from '../components/Content'
import EditForm from '../components/EditForm'
import AddForm from '../components/AddForm'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Content',
      component: Content
    },
    {
      path: '/edit',
      name: 'Edit',
      component: EditForm
    },
    {
      path: '/add',
      name: 'Add',
      component: AddForm
    }
  ]
})
